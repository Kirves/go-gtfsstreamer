package gtfsstreamer

import (
	"testing"
)

var (
	rc *RouteCollection
)

func TestNewRouteCollection(t *testing.T) {
	fn := "feed/routes.txt"
	rc, err := NewRouteCollection(fn)
	if err != nil {
		t.Fatal(err)
	}
	if len(rc.Routes) != 697 {
		t.Fatal("Wrong number of routes:", len(rc.Routes))
	}
}
