package gtfsstreamer

import (
	_ "fmt"
	"testing"
	"time"
)

var (
	f *GTFSFeed
)

func TestFeedGeneration(t *testing.T) {
	feed, err := NewGTFSFeed("test_feed")
	if err != nil {
		t.Fatal(err)
	}
	f = feed
}

func TestNewStreamer(t *testing.T) {
	stream := NewStreamer(f)
	stream.BlockUntilNextRide = false
	stream.PrefetchNextDay = true
	//stream.ApplyFilterFunction(func(te *TripEntity) bool { return te.Trip.Route.Id == "1" })

	err := stream.Seek(time.Date(2013, time.July, 19, 15, 0, 0, 0, time.UTC))
	if err == nil {
		t.Fatal("Did not generate error on wrong date")
	}
	err = stream.Seek(time.Date(2013, time.April, 19, 15, 0, 0, 0, time.UTC))
	if err == nil {
		t.Fatal("Did not generate error on wrong date")
	}

	stream.Seek(time.Date(2013, time.June, 19, 12, 0, 0, 0, time.UTC))
	t.Log("Prefetch threshold:", stream.stream.prefetchThreshold)
	i := 0
	for {
		te := stream.Next()
		if te == nil || i >= 10 {
			break
		}
		t.Log("Popping item:", te)
		i++
	}
}

func TestNewAsyncStreamer(t *testing.T) {
	stream := NewAsyncStreamer(f, func(te *TripEntity) { t.Log("Popped item:", te) })
	//stream.ApplyFilterFunction(func(te *TripEntity) bool { return te.Trip.Route.Id == "1" })

	err := stream.Seek(time.Date(2013, time.July, 19, 15, 0, 0, 0, time.UTC))
	if err == nil {
		t.Fatal("Did not generate error on wrong date")
	}
	err = stream.Seek(time.Date(2013, time.April, 19, 15, 0, 0, 0, time.UTC))
	if err == nil {
		t.Fatal("Did not generate error on wrong date")
	}

	stream.Seek(time.Date(2013, time.June, 19, 12, 0, 0, 0, time.UTC))
	stream.StartStreaming()
	<-stream.C
	//	select {
	//	case <-stream.C:
	//		t.Log("Streaming ended")
	//	case <-time.After(2 * time.Second):
	//		t.Log("Hearthbeat")
	//	}
	//time.Sleep(10 * time.Second)
	//stream.StopStreaming()
	//	for stream.streaming {
	//		time.Sleep(2 * time.Second)
	//	}
	if stream.streaming {
		t.Fatal("Did not termminate properly")
	}
}
