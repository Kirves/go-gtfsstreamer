package gtfsstreamer

import (
	"encoding/csv"
	"errors"
	"io"
	"os"
	"strconv"
)

type Node struct {
	Id   string
	Code string
	Name string
	Desc string
	Lat  float64
	Lon  float64
}

type NodeCollection struct {
	nodes map[string]Node // id -- Node
}

func NewNodeCollection(fname string) (NodeCollection, error) {
	ret := NodeCollection{}
	ret.nodes = make(map[string]Node)
	file, err := os.Open(fname)
	if err != nil {
		return NodeCollection{}, err
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.TrailingComma = true

	// skip first line
	reader.Read()

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return NodeCollection{}, err
		}
		nLat, _ := strconv.ParseFloat(record[4], 64)
		nLon, _ := strconv.ParseFloat(record[5], 64)
		tmp := Node{
			record[0],
			record[1],
			record[2],
			record[3],
			nLat,
			nLon}

		ret.nodes[tmp.Id] = tmp
	}
	return ret, nil
}

func (n *NodeCollection) NodeFromId(id string) (Node, error) {
	if v, ok := n.nodes[id]; ok {
		return v, nil
	}
	return Node{}, errors.New("Missing node in collection.")
}
