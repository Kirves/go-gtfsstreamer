package gtfsstreamer

import (
	"testing"
	"time"
)

func TestEntitiesGeneration(t *testing.T) {
	c, _ := NewCalendar("test_feed/calendar.txt")
	nc, _ := NewNodeCollection("test_feed/stops.txt")
	tc, _ := NewTripCollection("test_feed/trips.txt", &c, nil, &nc)
	tc.PopulateStopTimes("test_feed/stop_times.txt")

	aa := tc.GetEntitiesForDay(time.Date(2013, time.June, 20, 15, 0, 0, 0, time.UTC))
	if len(aa) != 7 {
		t.Fatal("Wrong number of entities:", len(aa))
	}
	t.Log("Start date:", aa[0].StartDate)
	t.Log("Stops:", aa[0].Trip.Stops)
}

func TestEmptyEntitiesGeneration(t *testing.T) {
	c, _ := NewCalendar("test_feed/calendar.txt")
	nc, _ := NewNodeCollection("test_feed/stops.txt")
	tc, _ := NewTripCollection("test_feed/trips.txt", &c, nil, &nc)
	tc.PopulateStopTimes("test_feed/stop_times.txt")

	aa := tc.GetEntitiesForDay(time.Date(2013, time.August, 20, 15, 0, 0, 0, time.UTC))
	if len(aa) != 0 {
		t.Fatal("Wrong number of entities:", len(aa))
	}
}
