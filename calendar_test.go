package gtfsstreamer

import (
	"testing"
	"time"
)

var (
	c Calendar
)

func TestNewCalendar(t *testing.T) {
	fn := "feed/calendar.txt"
	calendar, err := NewCalendar(fn)
	if err != nil {
		t.Fatal(err)
	}
	if len(calendar.Entries) != 39 {
		t.Fatal("Wrong number of entries wrt file:", len(calendar.Entries))
	}
	if !calendar.MinDate.Equal(time.Date(2013, time.June, 06, 0, 0, 0, 0, time.UTC)) {
		t.Fatal("Min date is wrong:", calendar.MinDate)
	}
	if !calendar.MaxDate.Equal(time.Date(2013, time.June, 30, 0, 0, 0, 0, time.UTC)) {
		t.Fatal("Max date is wrong:", calendar.MaxDate)
	}
	c = calendar
}

func TestCheckIds(t *testing.T) {
	date, _ := time.Parse("20060102", "20130615")
	ids := c.IdsForDate(date)
	if len(ids) != 10 {
		t.Error("Wrong number of ids:", len(ids))
	}
	t.Log("IDS:", ids)
}
