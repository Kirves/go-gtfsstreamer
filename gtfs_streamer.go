package gtfsstreamer

import (
	"container/heap"
	"errors"
	log "github.com/op/go-logging"
	"path"
	"time"
)

var (
	NOFILTER_FUNC               = func(te *TripEntity) bool { return true } // always returns true -- accept any trip entity
	NOWAIT_FUNC                 = func(te *TripEntity, t time.Time) { return }
	DEFAULT_PRE_GENERATION_RIDE = 0 * time.Minute
	PREFETCH_THRESHOLD          = 0.5 // %
)

type GTFSFeed struct {
	calendar *Calendar
	trips    *TripCollection
	routes   *RouteCollection
	nodes    *NodeCollection
}

type TripQueue []*TripEntity

type StreamerCore struct {
	currentDate       time.Time
	feed              *GTFSFeed
	queue             *TripQueue
	filterFunc        func(te *TripEntity) bool
	prefetchThreshold int
}

var logger *log.Logger = nil

func UseLogger(l *log.Logger) {
	logger = l
}

func newStreamerCore(feed *GTFSFeed) *StreamerCore {
	ret := new(StreamerCore)
	ret.feed = feed
	ret.currentDate = time.Now()
	// set queue (heap)
	ret.queue = &TripQueue{}
	heap.Init(ret.queue)
	// set filter function
	ret.filterFunc = NOFILTER_FUNC

	return ret
}

func (sc *StreamerCore) len() int {
	return sc.queue.Len()
}

func (sc *StreamerCore) needsPrefetch() bool {
	return sc.queue.Len() <= sc.prefetchThreshold
}

func (sc *StreamerCore) populateForDay(t time.Time) {
	if t.After(sc.feed.calendar.MaxDate) {
		return
	}
	ents := make([]TripEntity, 0)
	ents = sc.feed.trips.GetEntitiesForDay(t)
	for len(ents) == 0 {
		t = t.AddDate(0, 0, 1)
		if t.After(sc.feed.calendar.MaxDate) {
			break
		}
		ents = sc.feed.trips.GetEntitiesForDay(t)
	}
	for i, _ := range ents {
		heap.Push(sc.queue, &(ents[i]))
	}
	// set current date to keep it stable
	sc.currentDate = t

	// remember to set new threshold
	sc.prefetchThreshold = (int)((float64)(len(ents)) * PREFETCH_THRESHOLD)
}

func (sc *StreamerCore) populateForNextDay() {
	sc.populateForDay(sc.currentDate.AddDate(0, 0, 1))
}

func (sc *StreamerCore) seek(t time.Time) error {
	if t.Before(sc.feed.calendar.MinDate) {
		return errors.New("Seek date is before minumum date for service calendar.")
	}
	if t.After(sc.feed.calendar.MaxDate) {
		return errors.New("Seek date is after maximum date for service calendar.")
	}

	// generate all entities for date
	// filter out the ones before my time
	ents := sc.feed.trips.GetEntitiesForDay(t)
	for i, v := range ents {
		if t.Before(v.StartDate) {
			heap.Push(sc.queue, &(ents[i]))
		}
	}
	sc.currentDate = t
	sc.prefetchThreshold = (int)((float64)(len(ents)) * PREFETCH_THRESHOLD)
	return nil
}

func (sc *StreamerCore) getItem(prefetch bool) *TripEntity {
	ret := heap.Pop(sc.queue).(*TripEntity)
	if prefetch && sc.needsPrefetch() {
		sc.populateForNextDay()
	}
	return ret
}

func (sc *StreamerCore) applyFilter(f func(te *TripEntity) bool) {
	sc.filterFunc = f
}

// HEAP STUFF
func (tq TripQueue) Len() int {
	return len(tq)
}

func (tq TripQueue) Less(i, j int) bool {
	return tq[i].StartDate.Before(tq[j].StartDate)
}

func (tq TripQueue) Swap(i, j int) {
	tq[i], tq[j] = tq[j], tq[i]
}

func (tq *TripQueue) Push(x interface{}) {
	*tq = append(*tq, x.(*TripEntity))
}

func (tq *TripQueue) Pop() interface{} {
	old := *tq
	n := len(old)
	x := old[n-1]
	*tq = old[0 : n-1]
	return x
}

// END

func NewGTFSFeed(dir string) (*GTFSFeed, error) {
	ret := new(GTFSFeed)
	c_file := path.Join(dir, "calendar.txt")
	tc_file := path.Join(dir, "trips.txt")
	rc_file := path.Join(dir, "routes.txt")
	st_file := path.Join(dir, "stop_times.txt")
	nc_file := path.Join(dir, "stops.txt")

	if logger != nil {
		logger.Info("Parsing calendar file: %s", c_file)
	}
	c, err := NewCalendar(c_file)
	if err != nil {
		return nil, err
	}
	if logger != nil {
		logger.Info("Parsing routes file: %s", rc_file)
	}
	rc, err := NewRouteCollection(rc_file)
	if err != nil {
		return nil, err
	}
	if logger != nil {
		logger.Info("Parsing nodes file: %s", nc_file)
	}
	nc, err := NewNodeCollection(nc_file)
	if err != nil {
		return nil, err
	}
	if logger != nil {
		logger.Info("Parsing trips file: %s", tc_file)
		logger.Info("Creating trip collection")
	}
	tc, err := NewTripCollection(tc_file, &c, rc, &nc)
	if err != nil {
		return nil, err
	}
	if logger != nil {
		logger.Info("Populating stop times: %s", st_file)
	}
	err = tc.PopulateStopTimes(st_file)
	if err != nil {
		return nil, err
	}
	ret.calendar = &c
	ret.routes = rc
	ret.trips = tc
	ret.nodes = &nc
	return ret, nil
}

// AsyncStreamer

type AsyncStreamer struct {
	stream            *StreamerCore
	callback          func(te *TripEntity)
	RidePreGeneration time.Duration // How much time before ride starting time the ride has to be generated
	C                 chan bool
	streaming         bool
	stopStream        chan bool
	waitCallback      func(te *TripEntity, t time.Time)
}

func NewAsyncStreamer(feed *GTFSFeed, cb func(te *TripEntity)) *AsyncStreamer {
	ret := new(AsyncStreamer)
	ret.callback = cb
	ret.stream = newStreamerCore(feed)
	ret.RidePreGeneration = DEFAULT_PRE_GENERATION_RIDE
	ret.C = make(chan bool, 1)
	ret.stopStream = make(chan bool, 1)
	ret.streaming = false
	ret.waitCallback = NOWAIT_FUNC
	return ret
}

func (g *AsyncStreamer) Seek(t time.Time) error {
	return g.stream.seek(t)
}

func (g *AsyncStreamer) Length() int {
	return g.stream.len()
}

func (g *AsyncStreamer) populateForNextDay() {
	g.stream.populateForNextDay()
}

func (g *AsyncStreamer) ApplyFilterFunction(f func(te *TripEntity) bool) {
	g.stream.applyFilter(f)
}

func (g *AsyncStreamer) ApplyWaitCallback(f func(te *TripEntity, t time.Time)) {
	g.waitCallback = f
}

func (g *AsyncStreamer) startStream() {
	for {
		var ret *TripEntity
		for {
			if g.Length() == 0 {
				g.C <- true
				g.streaming = false
				return
			}
			ret = g.stream.getItem(true)
			if g.stream.filterFunc(ret) {
				break
			}
		}

		notifyDate := ret.StartDate.Add(-g.RidePreGeneration)
		now := time.Now()
		if notifyDate.Before(now) || notifyDate.Equal(now) {
			select {
			case <-g.stopStream:
				return
			default:
				g.callback(ret)
			}
		} else {
			wait := notifyDate.Sub(now)
			timer := time.NewTimer(wait)
			g.waitCallback(ret, notifyDate)
			// timer := time.NewTimer(time.Second)
			// check if StopStream() was called
			select {
			case <-g.stopStream:
				return
			case <-timer.C:
				g.callback(ret)
			}
			timer.Stop()
		}
	}
}

// StartStreaming calls the callback function every time a new ride has to be generated.
// This function is not blocking, so it immediately returns. When no rides are available a true value is sent on the internal channel to notify it.
func (g *AsyncStreamer) StartStreaming() {
	g.streaming = true
	go g.startStream()
}

// Stop streaming
func (g *AsyncStreamer) StopStreaming() {
	g.stopStream <- true
	g.streaming = false
}

// STREAMER

type Streamer struct {
	stream             *StreamerCore
	BlockUntilNextRide bool          // Next() method blocks until the next ride has to be generated
	RidePreGeneration  time.Duration // How much time before ride starting time the ride has to be generated. Invalid if BlockUntilNextRide is false.
	PrefetchNextDay    bool
}

func NewStreamer(feed *GTFSFeed) *Streamer {
	ret := new(Streamer)
	ret.stream = newStreamerCore(feed)
	ret.BlockUntilNextRide = false
	ret.RidePreGeneration = DEFAULT_PRE_GENERATION_RIDE
	return ret
}

func (g *Streamer) Seek(t time.Time) error {
	return g.stream.seek(t)
}

// NOTE: Length method may yield invalid count if prefetch is enabled
func (g *Streamer) Length() int {
	return g.stream.len()
}

func (g *Streamer) populateForNextDay() {
	g.stream.populateForNextDay()
}

func (g *Streamer) ApplyFilterFunction(f func(te *TripEntity) bool) {
	g.stream.applyFilter(f)
}

// Next returns the next ride based on the options set.
// Returns nil if no other rides are present
func (g *Streamer) Next() *TripEntity {
	var ret *TripEntity
	for {
		if g.Length() == 0 {
			return nil
		}
		ret = g.stream.getItem(g.PrefetchNextDay)
		if g.stream.filterFunc(ret) {
			break
		}
	}

	// if BlockUntileNextRide is not set just return a ride
	if !g.BlockUntilNextRide {
		return ret
	}
	notifyDate := ret.StartDate.Add(-g.RidePreGeneration)
	now := time.Now()
	if notifyDate.Before(now) || notifyDate.Equal(now) {
		return ret
	}
	wait := notifyDate.Sub(now)
	timer := time.NewTimer(wait)
	//timer := time.NewTimer(5 * time.Second) // FOR TEST
	<-timer.C
	return ret
}
