package gtfsstreamer

import (
	"testing"
)

func TestNewNodeCollection(t *testing.T) {
	nc, err := NewNodeCollection("test_feed/stops.txt")
	if err != nil {
		t.Fatal(err)
	}
	id := "10841"
	notId := "ksdjf"
	n, err := (&nc).NodeFromId(notId)
	if err == nil {
		t.Fatal("Did not return error on non-existing node")
	}
	n, err = (&nc).NodeFromId(id)
	if err != nil {
		t.Fatal("Returned error on existing node")
	}
	if n.Id != id {
		t.Fatal("Wrong id:", n.Id, "vs", id)
	}
	if n.Lat == 0 {
		t.Fatal("Missing latitude")
	}
}
