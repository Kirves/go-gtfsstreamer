package gtfsstreamer

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Stop struct {
	StopId              string
	StopNum             int
	StartHour, StartMin int
	EndHour, EndMin     int
	Info                *Node
}

type TripEntry struct {
	Route *RouteStruct
	//Service       *CalendarEntry
	TripId        string
	TripShortName string
	Direction     string
	Stops         []Stop
}

type TripCollection struct {
	Trips   []TripEntry
	Map     map[string][]int
	Service *Calendar
	Nodes   *NodeCollection
}

type TripEntity struct {
	StartDate          time.Time
	EntityGenerateDate time.Time
	Trip               *TripEntry
}

func NewTripCollection(fn string, c *Calendar, r *RouteCollection, n *NodeCollection) (*TripCollection, error) {
	ret := new(TripCollection)
	ret.Map = make(map[string][]int)
	ret.Trips = make([]TripEntry, 0, 1)
	ret.Service = c
	ret.Nodes = n
	file, err := os.Open(fn)
	if err != nil {
		return new(TripCollection), err
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.TrailingComma = true

	// skip first line
	reader.Read()

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return new(TripCollection), err
		}

		tmp := TripEntry{}
		tmp.Stops = make([]Stop, 0, 1)
		if r != nil {
			tmp.Route = r.GetRouteById(record[0])
		} else {
			tmp.Route = new(RouteStruct)
		}
		tmp.TripId = record[2]
		tmp.TripShortName = record[4]
		tmp.Direction = record[5]

		l := len(ret.Trips)
		if l == cap(ret.Trips) {
			tmpAr := make([]TripEntry, l, l*2)
			copy(tmpAr, ret.Trips)
			ret.Trips = tmpAr
		}
		ret.Trips = ret.Trips[0 : l+1]
		ret.Trips[l] = tmp
		if c != nil {
			sid := c.EntryForId(record[1])
			if sid != nil {
				if _, ok := ret.Map[sid.Id]; !ok {
					ret.Map[sid.Id] = make([]int, 0, 1)
				}
				l2 := len(ret.Map[sid.Id])
				if l2 == cap(ret.Map[sid.Id]) {
					tmpAr2 := make([]int, l2, l2*2)
					copy(tmpAr2, ret.Map[sid.Id])
					ret.Map[sid.Id] = tmpAr2
				}
				ret.Map[sid.Id] = ret.Map[sid.Id][0 : l2+1]
				ret.Map[sid.Id][l2] = l
			}
		}
	}
	return ret, nil
}

//func (t TripEntry) IsTripValidForDate(day time.Time) bool {
//	return t.Service.IsCompatibleWithDate(day)
//}

// SORTING STUFF
type stopSorter struct {
	stops []Stop
}

func (s *stopSorter) Len() int {
	return len(s.stops)
}

func (s *stopSorter) Swap(i, j int) {
	s.stops[i], s.stops[j] = s.stops[j], s.stops[i]
}

func (s *stopSorter) Less(i, j int) bool {
	return s.stops[i].StopNum < s.stops[j].StopNum
}

type entitySorter struct {
	entities []TripEntity
}

func (e *entitySorter) Len() int {
	return len(e.entities)
}

func (e *entitySorter) Swap(i, j int) {
	e.entities[i], e.entities[j] = e.entities[j], e.entities[i]
}

func (e *entitySorter) Less(i, j int) bool {
	return e.entities[i].StartDate.Before(e.entities[j].StartDate)
}

// END

func (tc *TripCollection) PopulateStopTimes(fn string) error {
	// build map for trips (faster than searching)
	tmpMap := make(map[string]int)
	for i, v := range tc.Trips {
		tmpMap[v.TripId] = i
	}

	// parse file and populate list
	file, err := os.Open(fn)
	if err != nil {
		return err
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.TrailingComma = true

	// skip first line
	reader.Read()
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		id := record[0]
		var shour, smin, ehour, emin int
		if record[1] == "" {
			shour, smin, ehour, emin = -1, -1, -1, -1
		} else {
			shour, smin = parseStationTime(record[1])
			ehour, emin = parseStationTime(record[2])
		}
		stopid := record[3]
		stopnum, err := strconv.Atoi(record[4])
		if err != nil {
			continue
			//return err
		}
		nodeInfo, err := tc.Nodes.NodeFromId(stopid)
		var stop Stop
		if err == nil {
			stop = Stop{stopid, stopnum, shour, smin, ehour, emin, &nodeInfo}
		} else {
			fmt.Println("Missing node info for node", stopid, ":", err)
			stop = Stop{stopid, stopnum, shour, smin, ehour, emin, nil}
		}
		if i, ok := tmpMap[id]; ok {
			tc.Trips[i].addStopTime(stop)
		}
	}
	for i, _ := range tc.Trips {
		tc.Trips[i].FillStopTimeGaps()
	}
	return nil
}

func (t *TripEntry) FillStopTimeGaps() {
	var firstValidStart time.Time
	var firstValidEnd time.Time
	var lastValidStart time.Time
	//var prevDt time.Time
	//var lastValidEnd time.Time
	var hole bool = false
	now := time.Now()
	var missingFirst int = -1
	var missingLast int = -1
	for i, _ := range t.Stops {
		if t.Stops[i].StartHour != -1 {
			if hole {
				lastValidStart = time.Date(now.Year(), now.Month(), now.Day(), t.Stops[i].StartHour, t.Stops[i].StartMin, 0, 0, time.Local)
				//lastValidEnd = time.Date(now.Year(), now.Month(), now.Day(), t.Stops[i].EndHour, t.Stops[i].EndMin, 0, 0, time.UTC)
				gap := missingLast - missingFirst + 1
				timeGap := lastValidStart.Sub(firstValidStart)
				prevDt := firstValidStart
				dtEnd := firstValidEnd
				var dt time.Time
				//timeStep := int(math.Floor(timeGap.Minutes() / float64(gap)))
				timeStep := timeGap.Minutes() / float64(gap+1)
				j := 0
				//fmt.Println("Filling gap:", t.Stops[missingFirst-1].Info.Id, " :: ", t.Stops[missingFirst-1].StartHour, ":", t.Stops[missingFirst-1].StartMin)
				for j < gap {
					currStep := int(math.Floor(timeStep * float64(j+1)))
					//dt = dt.Add(time.Duration(timeStep) * time.Minute)
					dt = firstValidStart.Add(time.Duration(currStep) * time.Minute)
					//dtEnd = dtEnd.Add(time.Duration(timeStep) * time.Minute)
					dtEnd = firstValidEnd.Add(time.Duration(currStep) * time.Minute)
					//hourGapStart := (int)(dt.Sub(firstValidStart).Hours())
					var hourGapStart int = 0
					if dt.Day() != prevDt.Day() {
						hourGapStart = 24
					}
					//t.Stops[missingFirst+j].StartHour = hourGapStart + t.Stops[missingFirst-1].StartHour
					t.Stops[missingFirst+j].StartHour = hourGapStart + dt.Hour()
					//t.Stops[missingFirst+j].EndHour = hourGapStart + t.Stops[missingFirst-1].EndHour
					t.Stops[missingFirst+j].EndHour = hourGapStart + dtEnd.Hour()
					t.Stops[missingFirst+j].StartMin = dt.Minute()
					t.Stops[missingFirst+j].EndMin = dtEnd.Minute()
					//fmt.Println("Filling gap:", t.Stops[missingFirst+j].Info.Id, " :: ", t.Stops[missingFirst+j].StartHour, ":", t.Stops[missingFirst+j].StartMin)
					j++
					prevDt = dt
				}
				//fmt.Println("Filling gap:", t.Stops[missingFirst+gap].Info.Id, " :: ", t.Stops[missingFirst+gap].StartHour, ":", t.Stops[missingFirst+gap].StartMin)
				//fmt.Println("----------")
				missingFirst = -1
				missingLast = -1
				hole = false
			}
			firstValidStart = time.Date(now.Year(), now.Month(), now.Day(), t.Stops[i].StartHour, t.Stops[i].StartMin, 0, 0, time.Local)
			firstValidEnd = time.Date(now.Year(), now.Month(), now.Day(), t.Stops[i].EndHour, t.Stops[i].EndMin, 0, 0, time.Local)
		} else {
			// empty field
			// save indexes and go on
			if missingFirst != -1 {
				// we already have another empty field before
				// just move the missingLast boundary
				missingLast = i
			} else {
				missingFirst = i
				missingLast = i
			}
			hole = true
		}
	}
}

func parseStationTime(st string) (hour, min int) {
	tmp := strings.Split(st, ":")
	hour, _ = strconv.Atoi(tmp[0])
	min, _ = strconv.Atoi(tmp[1])
	return
}

func (tc *TripCollection) GetEntitiesForDay(day time.Time) []TripEntity {
	// get service ids from calendar
	ids := tc.Service.IdsForDate(day)
	total_trips := 0
	for _, v := range ids {
		total_trips += len(tc.Map[v])
	}
	ret := make([]TripEntity, 0, total_trips)

	// generate entities for each service based on the day
	for _, v := range ids {
		for _, k := range tc.Map[v] {
			te := tc.Trips[k]
			val, err := te.GenerateEntity(day)
			if err != nil {
				continue
			}
			ret = ret[0 : len(ret)+1]
			ret[len(ret)-1] = val
		}
	}
	//es := entitySorter{ret}
	//sort.Sort(&es)
	return ret
}

func forgeDate(d time.Time, hour int, min int, fix bool) time.Time {
	if fix {
		if hour >= 24 {
			return time.Date(d.Year(), d.Month(), d.Day(), hour-24, min, 0, 0, time.Local)
		} else {
			return time.Date(d.Year(), d.Month(), d.Day(), hour, min, 0, 0, time.Local)
		}
	} else {
		return time.Date(d.Year(), d.Month(), d.Day(), hour, min, 0, 0, time.Local)
	}
}

func (te *TripEntry) GenerateEntity(t time.Time) (TripEntity, error) {
	ret := TripEntity{}
	if len(te.Stops) > 0 {
		ret.StartDate = forgeDate(t, te.Stops[0].StartHour, te.Stops[0].StartMin, false)
		ret.EntityGenerateDate = forgeDate(t, te.Stops[0].StartHour, te.Stops[0].StartMin, true)
	} else {
		return TripEntity{}, errors.New("No stops for this trip.")
	}
	ret.Trip = te
	return ret, nil
}

func (t *TripEntry) addStopTime(s Stop) {
	l := len(t.Stops)
	if l == cap(t.Stops) {
		tmpAr := make([]Stop, l, l*2)
		copy(tmpAr, t.Stops)
		t.Stops = tmpAr
	}
	t.Stops = t.Stops[0 : l+1]
	t.Stops[l] = s

	// keep this sorted. Is there a better way?
	ss := stopSorter{t.Stops}
	sort.Sort(&ss)
}
