package gtfsstreamer

import (
	"encoding/csv"
	"io"
	"os"
	"time"
)

const (
	MONDAY = 1 << iota
	TUESDAY
	WEDNESDAY
	THURSDAY
	FRIDAY
	SATURDAY
	SUNDAY
)

var (
	DayMapping     = map[int]uint{1: MONDAY, 2: TUESDAY, 3: WEDNESDAY, 4: THURSDAY, 5: FRIDAY, 6: SATURDAY, 7: SUNDAY}
	TimeDayMapping = map[time.Weekday]uint{
		time.Monday:    MONDAY,
		time.Tuesday:   TUESDAY,
		time.Wednesday: WEDNESDAY,
		time.Thursday:  THURSDAY,
		time.Friday:    FRIDAY,
		time.Saturday:  SATURDAY,
		time.Sunday:    SUNDAY}
)

type CalendarEntry struct {
	Id        string
	Days      uint
	StartDate time.Time
	EndDate   time.Time
}

type Calendar struct {
	Entries []CalendarEntry
	MinDate time.Time
	MaxDate time.Time
}

func NewCalendar(fn string) (Calendar, error) {
	ret := Calendar{}
	ret.Entries = make([]CalendarEntry, 0, 1)
	minDateInit := false
	calLongForm := "20060102"
	file, err := os.Open(fn)
	if err != nil {
		return Calendar{}, err
	}
	defer file.Close()
	reader := csv.NewReader(file)

	// skip first line
	reader.Read()

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return Calendar{}, err
		}

		tmp := CalendarEntry{}
		tmp.Id = record[0]
		for _, i := range []int{1, 2, 3, 4, 5, 6, 7} {
			if record[i] == "1" {
				tmp.Days = tmp.Days | DayMapping[i]
			}
		}
		tmp.StartDate, _ = time.Parse(calLongForm, record[8])
		tmp.EndDate, _ = time.Parse(calLongForm, record[9])
		tmp.EndDate = tmp.EndDate.Add(24 * time.Hour).Add(-1 * time.Second)
		if tmp.EndDate.After(ret.MaxDate) {
			ret.MaxDate = tmp.EndDate
		}
		if !minDateInit || tmp.StartDate.Before(ret.MinDate) {
			ret.MinDate = tmp.StartDate
			minDateInit = true
		}

		l := len(ret.Entries)
		if l == cap(ret.Entries) {
			tmpAr := make([]CalendarEntry, l, l*2)
			copy(tmpAr, ret.Entries)
			ret.Entries = tmpAr
		}
		ret.Entries = ret.Entries[0 : l+1]
		ret.Entries[l] = tmp
		if logger != nil {
			logger.Debug("New calendar entry: %s :: %s :: %s", tmp.Id, tmp.StartDate, tmp.EndDate)
		}
	}
	return ret, nil
}

func (c *CalendarEntry) IsCompatibleWithDate(t time.Time) bool {
	day := t.Weekday()
	if c.Days&TimeDayMapping[day] == TimeDayMapping[day] {
		if c.StartDate.Before(t) || c.StartDate == t {
			if c.EndDate.After(t) || c.EndDate == t {
				return true
			}
		}
	}
	return false
}

//func (c *CalendarEntry) IsCompatibleWithDateRange(start time.Time, end time.Time) bool {
//	day := t.Weekday()
//	if c.Days&TimeDayMapping[day] == TimeDayMapping[day] {
//		if c.StartDate.Before(t) || c.StartDate == t {
//			if c.EndDate.After(t) || c.EndDate == t {
//				return true
//			}
//		}
//	}
//	return false
//}

func (c *Calendar) IdsForDate(t time.Time) []string {
	ret := make([]string, 0, len(c.Entries))
	for _, v := range c.Entries {
		if (&v).IsCompatibleWithDate(t) {
			ret = ret[0 : len(ret)+1]
			ret[len(ret)-1] = v.Id
		}
	}
	return ret
}

func (c Calendar) EntryForId(id string) *CalendarEntry {
	for _, v := range c.Entries {
		if v.Id == id {
			return &v
		}
	}
	return nil
}
