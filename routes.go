package gtfsstreamer

import (
	"encoding/csv"
	"io"
	"os"
	"strconv"
)

const (
	TRAM = iota
	METRO
	RAIL
	BUS
	FERRY
	CABLE_CAR
	GONDOLA
	FUNICOLAR
)

type RouteStruct struct {
	Id, Agency, ShortName, LongName string
	Type                            int
}

type RouteCollection struct {
	Routes []RouteStruct
	Map    map[string]int
}

func NewRouteCollection(fn string) (*RouteCollection, error) {
	ret := new(RouteCollection)
	ret.Map = make(map[string]int)
	ret.Routes = make([]RouteStruct, 0, 1)
	file, err := os.Open(fn)
	if err != nil {
		return new(RouteCollection), err
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.TrailingComma = true

	// skip first line
	reader.Read()

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return new(RouteCollection), err
		}

		tmp := RouteStruct{}
		tmp.Id = record[0]
		tmp.Agency = record[1]
		tmp.ShortName = record[2]
		tmp.LongName = record[3]
		if v, e := strconv.Atoi(record[5]); e != nil {
			continue
		} else {
			tmp.Type = v
		}

		l := len(ret.Routes)
		if l == cap(ret.Routes) {
			tmpAr := make([]RouteStruct, l, l*2)
			copy(tmpAr, ret.Routes)
			ret.Routes = tmpAr
		}
		ret.Routes = ret.Routes[0 : l+1]
		ret.Routes[l] = tmp
		ret.Map[tmp.Id] = l
	}
	return ret, nil
}

func (rc *RouteCollection) GetRouteById(id string) *RouteStruct {
	if v, ok := rc.Map[id]; ok {
		return &(rc.Routes[v])
	}
	return new(RouteStruct)
}
