package gtfsstreamer

import (
	"testing"
)

var (
	tc *TripCollection
)

func TestNewTripList(t *testing.T) {
	fn := "feed/trips.txt"
	rc, _ := NewRouteCollection("feed/routes.txt")
	nc, _ := NewNodeCollection("test_feed/stops.txt")
	trips, err := NewTripCollection(fn, nil, rc, &nc)
	if err != nil {
		t.Fatal(err)
	}
	if len(trips.Trips) != 97119 {
		t.Fatal("Wrong number of entries wrt file:", len(trips.Trips))
	}
	tmp := trips.Trips[0]
	if tmp.Route.Id != "1" {
		t.Fatal("Wrong route id:", tmp.Route.Id)
	}
	if tmp.TripId != "20130603_INVERNALE FESTIVO_fes_i_2g_1_12335343" {
		t.Fatal("Wrong trip id:", tmp.TripId)
	}
	if tmp.TripShortName != "Greco - Castelli" {
		t.Fatal("Wrong trip short name:", tmp.TripShortName)
	}
	if tmp.Direction != "0" {
		t.Fatal("Wrong direction:", tmp.Direction)
	}
	if len(trips.Map) > 0 {
		t.Fatal("Map length is higher than 0")
	}
	t.Log("Number of trips:", len(trips.Trips))
	t.Log("Number of mapped calendar services:", len(trips.Map))
	tc = trips
}

//func TestStopAddition(t *testing.T) {
//	tt := &(tc.Trips[0])
//	s1 := Stop{"id1", 5, 13, 20, 13, 20}
//	s2 := Stop{"id2", 0, 11, 15, 11, 15}
//	tt.addStopTime(s1)
//	tt.addStopTime(s2)
//	t.Log("Len:", len(tt.Stops))
//	t.Log("Cap:", cap(tt.Stops))
//	if tt.Stops[0].StopId != "id2" {
//		t.Fatal("Not ordered set")
//	}
//}

func TestStopTimes(t *testing.T) {
	fn := "feed/stop_times.txt"
	err := tc.PopulateStopTimes(fn)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("Tripid:", tc.Trips[0].TripId)
	if len(tc.Trips[0].Stops) != 14 {
		t.Fatal("Wrong number of stations:", len(tc.Trips[0].Stops))
	}
	prev_n := -1
	for _, v := range tc.Trips[0].Stops {
		if v.StopNum <= prev_n {
			t.Fatal("Unordered stations")
		}
		prev_n = v.StopNum
	}
	t.Log("Trips[0].Stops:", tc.Trips[0].Stops)
}
